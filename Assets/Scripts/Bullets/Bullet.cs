﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public enum tipoBullet { Normal, Fragmentadora, Teledirigida, Rebote }

	public Renderer BulletMesh;
	public GameObject ParticlesFire1;
	public GameObject ParticlesFire2;
	public GameObject ParticlesFire3;
	public GameObject ParticlesFire4;
	public Light Luz;

	[Space(3)]
	[Header("Opciones")]
	public GameObject ExplosionParticles;
	public int Damage;
	public tipoBullet TipoBullet = tipoBullet.Normal;

	[Space(3)]
	[Header("Fragmentadora")]
	public GameObject BulletDefault;
	public float Separacion;
	public float MaxForce;
	public int NumberBullets;

	[Space(3)]
	[Header("Teleridigido")]
	public float Radius;
	public float Vel;

	GameObject newExplosion;
	CountPoints ContadorPuntos;
	int Player;
	bool Teledirigido;

	int Rebotes;
	bool Explotar;

	GameObject[] newBullet;
	float Angles;
	Vector2 Direction;
	Vector3 Position;

	void Start () {

		if (TipoBullet == tipoBullet.Fragmentadora) {
			
			newBullet = new GameObject[NumberBullets];
			
			Angles = 360 / NumberBullets;
			
		}

	}

	public void PlayerShoot (int player, CountPoints Contador) 
	{

		Player = player;
		ContadorPuntos = Contador;

		BulletMesh.material = Resources.Load ("Materials/Bullets/Bullet " + (Player + 1)) as Material;

		if (Player == 0) 
		{
			ParticlesFire1.SetActive (true);
			Luz.color = Color.green;
		}
		if (Player == 1) 
		{
			ParticlesFire2.SetActive (true);
			Luz.color = Color.red;
		}
		if (Player == 2) 
		{
			ParticlesFire3.SetActive (true);
			Luz.color = Color.yellow;
		}
		if (Player == 3) 
		{
			ParticlesFire4.SetActive (true);
			Luz.color = Color.blue;
		}

	}

	public int returnPlayer () {

		return Player;

	}

	public bool isTeledirigido () {

		return Teledirigido;

	}
	
	void OnCollisionEnter(Collision enter) {

		if (enter.transform.tag == "Player") {
				
			if (enter.gameObject.GetComponent<Tanque> ().returnPlayer () != Player) {
					
				enter.gameObject.GetComponent<Health> ().Damage (Damage, Player);
					
			}
				
			if (TipoBullet == tipoBullet.Rebote) Explotar = true;
				
		}
			
		if (enter.transform.tag == "Barricada") {
				
			enter.gameObject.GetComponent<Barricada> ().Damage (Damage);
				
			if (TipoBullet == tipoBullet.Rebote) Explotar = true;
				
		}

		if (TipoBullet == tipoBullet.Fragmentadora) {

			Position = gameObject.transform.position;
			
			for (int i = 0; i < NumberBullets; i++) {
				
				Direction.x = Mathf.Cos (Mathf.Deg2Rad * (Angles * i));
				Direction.y = Mathf.Sin (Mathf.Deg2Rad * (Angles * i));
				
				newBullet [i] = (GameObject)Instantiate (BulletDefault);
				newBullet [i].transform.position = new Vector3 (Direction.x * Separacion + Position.x, Position.y, Direction.y * Separacion + Position.z);
				newBullet [i].GetComponent<Rigidbody> ().AddForce (new Vector3 (Direction.x * MaxForce, 0, Direction.y * MaxForce));
				
				newBullet [i].GetComponent<Bullet> ().PlayerShoot (Player, ContadorPuntos);
				
			}

		}

		if (TipoBullet == tipoBullet.Rebote) {

			if (Explotar || Rebotes >= 1) {
					
				newExplosion = (GameObject)Instantiate (ExplosionParticles);
					
				newExplosion.transform.position = transform.position;
					
				Destroy (gameObject);
					
			}
			else
			{
					
				Rebotes = Rebotes + 1;
					
			}

		}
		else
		{

			newExplosion = (GameObject)Instantiate (ExplosionParticles);
		
			newExplosion.transform.position = transform.position;
		
			Destroy (gameObject);

		}
		
	}

	void Update () {

		if (TipoBullet == tipoBullet.Teledirigida) {

			Teledirigido = true;

			Collider[] colliders = Physics.OverlapSphere (transform.position, Radius);
			
			foreach (Collider hit in colliders) {
				
				if (hit.tag == "Player") {
						
					if (hit.GetComponent<Tanque>().returnPlayer() != Player) {
							
						gameObject.GetComponent<Rigidbody>().position = Vector3.MoveTowards(gameObject.transform.position, hit.transform.position, Vel);

					}
					
				}
				
			}

		}

		if (TipoBullet == tipoBullet.Rebote) {
			
		}

		if (ContadorPuntos.EndGame () == false) {
			
			Destroy (gameObject);
			
		}

	}
	
}
