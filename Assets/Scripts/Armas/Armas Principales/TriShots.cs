﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TriShots : MonoBehaviour {
	
	public int Quality;
	public float BulletTime;
	public float MaxForce;
	public float SpaceBullet;
	
	GameObject[] newBullet;
	float subBulletTime;
	Vector2 Direccion;
	int subQuality;
	float Radianes;
	int Buffer;

	void Start () {
		
		newBullet = new GameObject[3];
		
	}

	public void TriShotsBullet (GameObject Bullet, CountPoints ContadorPuntos, GameObject PuntoDisparo, float RotationCañon, int Player, Text Texto) {
		
		if (subBulletTime >= BulletTime) {

			for (float i = -1; i < 2; i++) {
				
				Radianes = Mathf.Deg2Rad * (RotationCañon + (i * SpaceBullet));
				
				Direccion.x = Mathf.Sin(Radianes);
				Direccion.y = Mathf.Cos(Radianes);
				
				newBullet [Buffer] = (GameObject)Instantiate (Bullet);
				newBullet [Buffer].transform.position = PuntoDisparo.transform.position;
				newBullet [Buffer].GetComponent<Rigidbody>().AddForce (new Vector3 (Direccion.x * MaxForce, 0, Direccion.y * MaxForce));
				
				newBullet [Buffer].GetComponent<Bullet>().PlayerShoot(Player, ContadorPuntos);

				Buffer = Buffer + 1;
				
			}

			subBulletTime = 0;
			
			subQuality = subQuality + 1;

			Buffer = 0;
			
		}
		
		if (subBulletTime < BulletTime) {
			
			subBulletTime = subBulletTime + Time.deltaTime;
			
		}

		Texto.text = "Disparos Tripe " + subQuality + " / " + Quality;
		
	}
	
	public void ResetShot() {
		
		subQuality = 0;
		
		subBulletTime = BulletTime;
		
	}
	
	public bool EndShot () {
		
		if (subQuality >= Quality) {
			
			subQuality = 0;
			
			subBulletTime = BulletTime;
			
			return true;
			
		}
		else
		{
			
			return false;
			
		}
		
	}

}
