﻿using UnityEngine;
using System.Collections;

public class BulletDefault : MonoBehaviour {
	
	public float BulletTime;
	public float MaxForce;

	GameObject newBullet;
	float subBulletTime;
	Vector2 Direccion;

	public void ShotDefaultBullet (GameObject Bullet, CountPoints ContadorPuntos, GameObject PuntoDisparo, float RotationCañon, int Player) {

		if (subBulletTime >= BulletTime) {
			
			newBullet = (GameObject)Instantiate (Bullet);
			newBullet.transform.position = PuntoDisparo.transform.position;

			Direccion.x = Mathf.Sin(RotationCañon * Mathf.Deg2Rad);
			Direccion.y = Mathf.Cos(RotationCañon * Mathf.Deg2Rad);

			newBullet.GetComponent<Rigidbody>().AddForce (new Vector3 (Direccion.x * MaxForce, 0, Direccion.y * MaxForce));

			newBullet.GetComponent<Bullet>().PlayerShoot(Player, ContadorPuntos);
			
			subBulletTime = 0;
			
		}

		if (subBulletTime < BulletTime) {
			
			subBulletTime = subBulletTime + Time.deltaTime;
			
		}

	}
	
}
