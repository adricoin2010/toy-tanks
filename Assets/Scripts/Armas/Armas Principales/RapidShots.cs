﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RapidShots : MonoBehaviour {
	
	public int Quality;
	public float BulletTime;
	public float MaxForce;
	
	GameObject newBullet;
	float subBulletTime;
	Vector2 Direccion;
	int subQuality;

	public void RapidShotsBullet (GameObject Bullet, CountPoints ContadorPuntos, GameObject PuntoDisparo, float RotationCañon, int Player, Text Texto) {
		
		if (subBulletTime >= BulletTime) {
			
			newBullet = (GameObject)Instantiate (Bullet);
			newBullet.transform.position = PuntoDisparo.transform.position;
			
			Direccion.x = Mathf.Sin(RotationCañon * Mathf.Deg2Rad);
			Direccion.y = Mathf.Cos(RotationCañon * Mathf.Deg2Rad);
			
			newBullet.GetComponent<Rigidbody>().AddForce (new Vector3 (Direccion.x * MaxForce, 0, Direccion.y * MaxForce));
			
			newBullet.GetComponent<Bullet>().PlayerShoot(Player, ContadorPuntos);
			
			subBulletTime = 0;

			subQuality = subQuality + 1;
			
		}
		
		if (subBulletTime < BulletTime) {
			
			subBulletTime = subBulletTime + Time.deltaTime;
			
		}

		Texto.text = "Disparos Rapidos " + subQuality + " / " + Quality;
		
	}

	public void ResetShot() {

		subQuality = 0;

		subBulletTime = BulletTime;

	}

	public bool EndShot () {
		
		if (subQuality >= Quality) {

			subQuality = 0;

			subBulletTime = BulletTime;

			return true;
			
		}
		else
		{
			
			return false;
			
		}
		
	}

}
