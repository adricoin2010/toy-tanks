﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RafagaShots : MonoBehaviour {

	public int Quality;
	public float RafagaTime;
	public float BulletTime;
	public float MaxForce;
	
	GameObject newBullet;
	float subRafagaTime;
	float subBulletTime;
	Vector2 Direccion;
	int subQuality;
	int bufferQuality;
	
	public void RafagaShotsBullet (GameObject Bullet, CountPoints ContadorPuntos, GameObject PuntoDisparo, float RotationCañon, int Player, Text Texto) {
		
		if (subBulletTime >= BulletTime) {

			if (subRafagaTime >= RafagaTime) {

				newBullet = (GameObject)Instantiate (Bullet);
				newBullet.transform.position = PuntoDisparo.transform.position;
				
				Direccion.x = Mathf.Sin(RotationCañon * Mathf.Deg2Rad);
				Direccion.y = Mathf.Cos(RotationCañon * Mathf.Deg2Rad);
				
				newBullet.GetComponent<Rigidbody>().AddForce (new Vector3 (Direccion.x * MaxForce, 0, Direccion.y * MaxForce));
				
				newBullet.GetComponent<Bullet>().PlayerShoot(Player, ContadorPuntos);
				
				subRafagaTime = 0;

				bufferQuality = bufferQuality + 1;

				if (bufferQuality >= 3) {

					subBulletTime = 0;
					
					subQuality = subQuality + 1;

					bufferQuality = 0;

				}

			}
			
		}
		
		if (subBulletTime < BulletTime) {
			
			subBulletTime = subBulletTime + Time.deltaTime;
			
		}

		if (subRafagaTime < RafagaTime) {
			
			subRafagaTime = subRafagaTime + Time.deltaTime;
			
		}
		
		Texto.text = "Disparo Rafaga " + subQuality + " / " + Quality;
		
	}
	
	public void ResetShot() {
		
		subQuality = 0;
		
		subBulletTime = BulletTime;
		
	}
	
	public bool EndShot () {
		
		if (subQuality >= Quality) {
			
			subQuality = 0;
			
			subBulletTime = BulletTime;
			
			return true;
			
		}
		else
		{
			
			return false;
			
		}
		
	}

}
