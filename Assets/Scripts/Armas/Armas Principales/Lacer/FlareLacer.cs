﻿using UnityEngine;
using System.Collections;

public class FlareLacer : MonoBehaviour {

	public ParticleSystem FlareObject;
	public ParticleSystem SmokeObject;
	public ParticleSystem SparksObject;

	bool Activated;

	public void EmitFlare () {

		Activated = true;

	}

	void Update () {

		if (Activated) {

			FlareObject.enableEmission = true;
			SmokeObject.enableEmission = true;
			SparksObject.enableEmission = true;

		}
		else
		{

			FlareObject.enableEmission = false;
			SmokeObject.enableEmission = false;
			SparksObject.enableEmission = false;

		}

		Activated = false;

	}

}
