﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Lacer : MonoBehaviour {

	public GameObject FlareLacer;
	public GameObject LacerObject;
	public GameObject LacerLine;
	public GameObject PointA;
	public GameObject PointB;

	[Space(3)]
	[Header("Opciones")]
	public float Damage;
	public int Quality;
	
	GameObject newFlareLacer;
	RaycastHit Impact;
	bool Activated;
	int subQuality;
	int Player;
	Text Texto;
	
	void Start () {
		
		newFlareLacer = (GameObject)Instantiate (FlareLacer);
		
	}
	
	public void LacerShots (bool status, int player, Text texto) {
		
		Activated = status;
		Player = player;
		Texto = texto;
		
	}
	
	public void ResetShot() {
		
		subQuality = 0;
		
	}
	
	public bool EndShot () {
		
		if (subQuality >= Quality) {
			
			subQuality = 0;
			
			return true;
			
		}
		else
		{
			
			return false;
			
		}
		
	}

	public void DestroyFlare() {

		Destroy(newFlareLacer);

	}
	
	void Update () {
		
		if (Activated) {
			
			if (LacerLine.GetComponent<Renderer>().enabled == false) {
				
				LacerLine.GetComponent<Renderer>().enabled = true;
				
			}
			
			if (Physics.Linecast (PointA.transform.position, PointB.transform.position, out Impact)) {
				
				LacerObject.transform.localScale = new Vector3 (1.5f, 1.5f, Impact.distance);
				
				if (Impact.transform.tag == "Player") {
					
					Impact.transform.GetComponent<Health>().Damage(Damage, Player);
					
				}
				else if (Impact.transform.tag == "Barricada") {
					
					Impact.transform.GetComponent<Barricada>().Damage(Damage);

				}
			
				newFlareLacer.transform.position = Impact.point;
				
				newFlareLacer.GetComponent<FlareLacer>().EmitFlare();

			}

			subQuality = subQuality + 1;

			Texto.text = "Lacer " + subQuality + " / " + Quality;
			
		}
		else 
		{
			
			LacerObject.transform.localScale = new Vector3 ( 1.5f , 1.5f, 300 );
			
			if (LacerLine.GetComponent<Renderer>().enabled == true) {
				
				LacerLine.GetComponent<Renderer>().enabled = false;
				
			}
			
		}
		
	}

}
