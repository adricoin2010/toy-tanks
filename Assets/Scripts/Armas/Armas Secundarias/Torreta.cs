﻿using UnityEngine;
using System.Collections;

public class Torreta : MonoBehaviour {

	public GameObject TorretaObject;
	public Renderer CuerpoMesh;
	public GameObject CañonObject;
	public Renderer CañonMesh;
	public Light Luz;
	public GameObject PuntoDisparo;

	[Space(3)]
	[Header("Opciones")]
	public GameObject Bullet;
	public GameObject Explosion;
	public float Radius;
	public int Quality;
	public float TorretaTime;
	public float BulletTime;
	public float MaxForce;

	int Player;
	CountPoints ContadorPuntos;
	GameObject newExplosion;
	GameObject newBullet;
	float subTorretaTime;
	float subBulletTime;
	float RotationCañon;
	Vector2 BufferPosition;
	Vector2 Direccion;
	int subQuality;

	GameObject[] Players;
	int bufferPlayers;
	int PlayerAttack;

	void Start () {

		Players = new GameObject[4];

	}

	public int returnPlayer () {
		
		return Player;
		
	}

	public void TorretaDrop (int player, CountPoints Contador) {
		
		Player = player;
		ContadorPuntos = Contador;
			
		CuerpoMesh.material = Resources.Load ("Materials/Players/Tanque " + (Player + 1)) as Material;
		CañonMesh.material = Resources.Load ("Materials/Players/Cañon " + (Player + 1)) as Material;
			
		if (Player == 0) Luz.color = Color.green;
		if (Player == 1) Luz.color = Color.red;
		if (Player == 2) Luz.color = Color.yellow;
		if (Player == 3) Luz.color = Color.blue;
		
	}

	void Update () {

		Collider[] colliders = Physics.OverlapSphere (TorretaObject.transform.position, Radius);

		bufferPlayers = 0;

		foreach (Collider hit in colliders) {
			
			if (hit.tag == "Player") {
				
				if (hit.GetComponent<Tanque>().returnPlayer() != Player) {
					
					Players[bufferPlayers] = hit.gameObject;

					bufferPlayers = bufferPlayers + 1;
					
				}
				
			}
			
		}

		if (bufferPlayers > 0) {
			
			BufferPosition.x = CañonObject.transform.position.x - Players[0].transform.position.x;
			BufferPosition.y = CañonObject.transform.position.z - Players[0].transform.position.z;
			
			RotationCañon = Mathf.Atan2(BufferPosition.x, BufferPosition.y);
			
			CañonObject.transform.localRotation = Quaternion.Euler (0, Mathf.Rad2Deg * RotationCañon, 0);
			
			if (subBulletTime >= BulletTime) {
				
				Direccion.x = Mathf.Sin (RotationCañon);
				Direccion.y = Mathf.Cos (RotationCañon);
				
				newBullet = (GameObject)Instantiate (Bullet);
				newBullet.transform.position = PuntoDisparo.transform.position;
				
				newBullet.GetComponent<Rigidbody> ().AddForce (new Vector3 (-Direccion.x * MaxForce, 0, -Direccion.y * MaxForce));
				
				newBullet.GetComponent<Bullet> ().PlayerShoot (Player, ContadorPuntos);
				
				subBulletTime = 0;
				
				subQuality = subQuality + 1;
				
			}
			
			if (subQuality >= Quality) {
				
				newExplosion = (GameObject)Instantiate (Explosion);
				
				newExplosion.transform.position = transform.position;
				
				Destroy (TorretaObject);
				
			}
			
		}

		if (ContadorPuntos.EndGame () == false) {
			
			Destroy (TorretaObject);
			
		}

		if (subTorretaTime >= TorretaTime) {

			newExplosion = (GameObject)Instantiate (Explosion);
			
			newExplosion.transform.position = transform.position;
			
			Destroy (TorretaObject);
			
		}
		
		if (subTorretaTime < TorretaTime) {
			
			subTorretaTime = subTorretaTime + Time.deltaTime;
			
		}

		if (subBulletTime < BulletTime) {
			
			subBulletTime = subBulletTime + Time.deltaTime;
			
		}
		
	}

}
