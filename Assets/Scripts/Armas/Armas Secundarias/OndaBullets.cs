﻿using UnityEngine;
using System.Collections;

public class OndaBullets : MonoBehaviour {

	public float Separacion;
	public float MaxForce;
	public int NumberBullets;

	GameObject[] newBullet;
	int Player;
	float Angles;
	Vector2 Direction;
	Vector3 Position;

	void Start () {

		newBullet = new GameObject[NumberBullets];

		Angles = 360 / NumberBullets;

	}

	public void Shot (GameObject Bullet, CountPoints ContadorPuntos, GameObject Cañon, int Player) {

		Position = Cañon.transform.position;

		for (int i = 0; i < NumberBullets; i++) {
			
			Direction.x = Mathf.Cos(Mathf.Deg2Rad * (Angles * i));
			Direction.y = Mathf.Sin(Mathf.Deg2Rad * (Angles * i));
			
			newBullet [i] = (GameObject)Instantiate (Bullet);
			newBullet [i].transform.position = new Vector3 (Direction.x * Separacion + Position.x, Position.y, Direction.y * Separacion + Position.z);
			newBullet [i].GetComponent<Rigidbody>().AddForce (new Vector3 (Direction.x * MaxForce, 0, Direction.y * MaxForce));
			
			newBullet [i].GetComponent<Bullet>().PlayerShoot(Player, ContadorPuntos);
			
		}

	}

}
