﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DesviaBalas : MonoBehaviour {

	public GameObject DesviaBalasObject;

	[Space(3)]
	[Header("Opciones")]
	public float Force;
	public float Radius;
	public int Quality;
	
	int subQuality;

	public void DesviaBalasShots (int Player, Text Texto) {

		Collider[] colliders = Physics.OverlapSphere (transform.position, Radius);
		
		foreach (Collider hit in colliders) {

			if (hit.tag == "Bullet") {

				if (hit.GetComponent<Bullet>().returnPlayer() != Player) {
				
					if (hit.GetComponent<Bullet>().isTeledirigido()) {

						hit.GetComponent<Rigidbody>().AddExplosionForce(Force * 8, transform.position, Radius);

					}
					else
					{

						hit.GetComponent<Rigidbody>().AddExplosionForce(Force, transform.position, Radius);

					}
				
				}

			}
			
		}

		subQuality = subQuality + 1;

		Texto.text = "Desvia Balas " + subQuality + " / " + Quality;

		DesviaBalasObject.SetActive(true);

		DesviaBalasObject.transform.Rotate(new Vector3(0, 10, 0));
		
	}

	public void Stop () {

		DesviaBalasObject.SetActive(false);

	}
	
	public void ResetShot() {
		
		subQuality = 0;

		DesviaBalasObject.SetActive(false);
		
	}
	
	public bool EndShot () {
		
		if (subQuality >= Quality) {
			
			subQuality = 0;

			DesviaBalasObject.SetActive(false);
			
			return true;
			
		}
		else
		{
			
			return false;
			
		}
		
	}

}
