﻿using UnityEngine;
using System.Collections;

public class Mina : MonoBehaviour {
	
	public GameObject MinaObject;
	public Renderer MinaMesh;
	public Light Luz;

	[Space(3)]
	[Header("Opciones")]
	public GameObject Explosion;
	public float Damage;
	public float MinaTime;

	int Player;
	CountPoints ContadorPuntos;
	GameObject newExplosion;
	float subMinaTime;

	public int returnPlayer () {
		
		return Player;
		
	}

	public void MinaDrop (int player, CountPoints Contador) {

		Player = player;
		ContadorPuntos = Contador;
			
		MinaMesh.material = Resources.Load ("Materials/Players/Tanque " + (Player + 1)) as Material;
			
		if (Player == 0) Luz.color = Color.green;
		if (Player == 1) Luz.color = Color.red;
		if (Player == 2) Luz.color = Color.yellow;
		if (Player == 3) Luz.color = Color.blue;

		
	}
	
	void OnTriggerStay (Collider enter) {

		if (enter.transform.tag == "Player") {
			
			if (enter.GetComponent<Tanque>().returnPlayer() != Player) {
				
				enter.GetComponent<Health>().Damage(Damage, Player);

				newExplosion = (GameObject)Instantiate (Explosion);
				
				newExplosion.transform.position = transform.position;
				
				Destroy (MinaObject);
				
			}
			
		}

	}

	void Update () {

		if (ContadorPuntos.EndGame () == false) {
			
			Destroy (MinaObject);
			
		}

		if (subMinaTime >= MinaTime) {

			newExplosion = (GameObject)Instantiate (Explosion);
			
			newExplosion.transform.position = transform.position;
			
			Destroy (MinaObject);
			
		}
		
		if (subMinaTime < MinaTime) {
			
			subMinaTime = subMinaTime + Time.deltaTime;
			
		}
		
	}

}
