﻿using UnityEngine;
using System.Collections;

public class Escudo : MonoBehaviour {

	public GameObject EscudoObject;
	public GameObject Explosion;
	public float EscudoTime;
	
	int Player;
	CountPoints ContadorPuntos;

	GameObject newExplosion;
	float subEscudoTime;
	
	public void EscudoDrop (int player, CountPoints Contador) {
		
		Player = player;
		ContadorPuntos = Contador;
		
	}
	
	void OnTriggerStay (Collider enter) {

		if (enter.tag == "Player") {

			if (enter.GetComponent<Tanque>().returnPlayer() != Player) {

				enter.GetComponent<Tanque>().Ralentizar();

			}

		}

		if (enter.tag == "Torreta") {

			if (enter.GetComponent<Torreta>().returnPlayer() != Player) {

				newExplosion = (GameObject)Instantiate (Explosion);
				
				newExplosion.transform.position = enter.transform.position;
				
				Destroy (enter.gameObject);

			}

		}

		if (enter.tag == "Mina") {
			
			if (enter.GetComponent<Mina>().returnPlayer() != Player) {
				
				newExplosion = (GameObject)Instantiate (Explosion);
				
				newExplosion.transform.position = enter.transform.position;
				
				Destroy (enter.gameObject);
				
			}
			
		}

		/*
		if (enter.tag == "Bullet") {
			
			if (enter.GetComponent<Bullet>().returnPlayer() != Player) {

				newExplosion = (GameObject)Instantiate (Explosion);
				
				newExplosion.transform.position = enter.transform.position;
				
				Destroy (enter.gameObject);
				
			}
			
		}
		*/

	}

	void Update () {

		if (ContadorPuntos.EndGame () == false) {
			
			Destroy (EscudoObject);
			
		}
		
		if (subEscudoTime >= EscudoTime) {
			
			Destroy (EscudoObject);
			
		}
		
		if (subEscudoTime < EscudoTime) {
			
			subEscudoTime = subEscudoTime + Time.deltaTime;
			
		}
		
	}

}
