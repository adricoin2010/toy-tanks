﻿using UnityEngine;
using System.Collections;

public class AtraeBalas : MonoBehaviour {

	public GameObject AtraeBalasObject;
	public GameObject OndasObject;
	public Renderer BaseMesh;
	public Light Luz;

	[Space(3)]
	[Header("Opciones")]
	public GameObject Explosion;
	public float Force;
	public float Radius;
	public float AtraeBalasTime;

	int Player;
	CountPoints ContadorPuntos;
	GameObject newExplosion;
	float subAtraeBalasTime;

	public void AtraeBalasDrop (int player, CountPoints Contador) {
		
		Player = player;
		ContadorPuntos = Contador;
		
		BaseMesh.material = Resources.Load ("Materials/Players/Tanque " + (Player + 1)) as Material;
		
		if (Player == 0) Luz.color = Color.green;
		if (Player == 1) Luz.color = Color.red;
		if (Player == 2) Luz.color = Color.yellow;
		if (Player == 3) Luz.color = Color.blue;
		
	}

	void Update () {
		
		Collider[] colliders = Physics.OverlapSphere (transform.position, Radius);
		
		foreach (Collider hit in colliders) {
			
			if (hit.tag == "Bullet") {
				
				if (hit.GetComponent<Bullet>().returnPlayer() != Player) {
						
					hit.GetComponent<Rigidbody>().AddExplosionForce(Force, transform.position, Radius);
					
				}
				
			}
			
		}

		OndasObject.transform.Rotate(new Vector3(0, 10, 0));

		if (ContadorPuntos.EndGame () == false) {
			
			Destroy (AtraeBalasObject);
			
		}
		
		if (subAtraeBalasTime >= AtraeBalasTime) {
			
			newExplosion = (GameObject)Instantiate (Explosion);
			
			newExplosion.transform.position = transform.position;
			
			Destroy (AtraeBalasObject);
			
		}
		
		if (subAtraeBalasTime < AtraeBalasTime) {
			
			subAtraeBalasTime = subAtraeBalasTime + Time.deltaTime;
			
		}
		
	}

}
