﻿using UnityEngine;
using System.Collections;

public class Barricada : MonoBehaviour {

	public GameObject BarricadaObject;
	public Renderer BarricadaMesh;

	[Space(3)]
	[Header("Opciones")]
	public GameObject Explosion;
	public int MaxHealth;

	GameObject newExplosion;
	CountPoints ContadorPuntos;
	float BufferHealth;

	void Start () {

		BufferHealth = MaxHealth;

	}

	public void Damage(float status) {

		BufferHealth = BufferHealth - status;

	}

	public void BarricadaDrop (int Player, CountPoints Contador) {

		ContadorPuntos = Contador;
			
		BarricadaMesh.material = Resources.Load ("Materials/Players/Tanque " + (Player + 1)) as Material;
		
	}

	void Update () {
		
		if (ContadorPuntos.EndGame () == false) {
			
			Destroy (BarricadaObject);
			
		}

		if (BufferHealth <= 0) 
		{

			newExplosion = (GameObject)Instantiate (Explosion);
			
			newExplosion.transform.position = transform.position;
			
			Destroy (BarricadaObject);
			
		}
		
	}

}
