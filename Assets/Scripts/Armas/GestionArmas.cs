﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GestionArmas : MonoBehaviour {

    public GameObject CañonObject;
    public GameObject PuntoDisparo;
	public GameObject Dropper;
	public GameObject DropperObject;

	[Space(3)]
	[Header("Tipos de Balas")]
	public GameObject BulletDefault;
	public GameObject BulletGrande;
	public GameObject BulletEspectral;
	public GameObject BulletFragmentadora;
	public GameObject BulletTeledirigida;
	public GameObject bulletRebota;
	public int TimeBulletEspeciales;
	public int BulletActual;

	[Space(3)]
	[Header("Armas Secundarias")]
	public GameObject Torreta;
	public GameObject Mina;
	public GameObject Barricada;
	public GameObject AtraeBalas;

	int Player;
	CountPoints ContadorPuntos;
	GameObject Interfaz;
	Text TextArma;
	Text TextArma2;
	Text TextBullet;

	GameObject Bullet;

	GameObject newObject;

	int TipoArma = 0;
	int TipoArma2 = 0;
	int TipoBullet = 0;

	float subTimeBullet;

    bool Disparar = false;
	bool Disparar2 = false;

    float RotationCañon;
    Vector2 JoystickDerecho;
	int subQuality;
	bool subDisparo2;

	public void DatosPlayer (int player, CountPoints Contador, GameObject interfaz, Text TextoArma, Text TextoArma2, Text TextoBullet) {
		
		Player = player;
		ContadorPuntos = Contador;
		Interfaz = interfaz;
		TextArma = TextoArma;
		TextArma2 = TextoArma2;
		TextBullet = TextoBullet;
		
	}

	////////////////////////////////////////////////////////////////////////////////////
	// Armas Primarias
    public void CambioArmaPrimaria (int arma)
    {

        TipoArma = arma;

		CañonObject.GetComponent<RapidShots>().ResetShot();
		CañonObject.GetComponent<TriShots>().ResetShot();
		CañonObject.GetComponent<Lacer>().ResetShot();
		CañonObject.GetComponent<Lacer>().LacerShots(false, Player, TextArma);
		CañonObject.GetComponent<RafagaShots>().ResetShot();

		if (arma == 1) {

			TextArma.text = "Disparo Rapido";

		}

		if (arma == 2) {

			TextArma.text = "Disparo Triple";

		}

		if (arma == 3) {

			TextArma.text = "Lacer";

		}

		if (arma == 4) {
			
			TextArma.text = "Disparo Rafaga";
			
		}

    }

	////////////////////////////////////////////////////////////////////////////////////
	// Armas Secundarias
	public void CambioArmaSecundaria (int arma)
	{
		
		TipoArma2 = arma;

		CañonObject.GetComponent<DesviaBalas>().ResetShot();

		subQuality = 0;

		DropperObject.SetActive(false);

		if (arma == 4) {
			
			TextArma2.text = "Desvia Balas";
			
		}
		
	}

	////////////////////////////////////////////////////////////////////////////////////
	// Balas Especiales
	public void CambioBullet (int bullet)
	{
		
		TipoBullet = bullet;
		
		subTimeBullet = 0;
		
	}

	////////////////////////////////////////////////////////////////////////////////////
    public void ApuntarCañon(Vector2 joystick, float rotation)
    {

        JoystickDerecho = joystick;
        RotationCañon = rotation;

    }

    public void DispararArma (bool status)
    {

        Disparar = status;

    }

	public void DispararArmaSecundaria (bool status)
	{
		
		Disparar2 = status;
		
	}
	
	void Update () {

		////////////////////////////////////////////////////////////////////////////////////
		// Armas Primarias
		if (TipoArma == 0) 
		{

			if (Disparar) 
			{

				CañonObject.GetComponent<BulletDefault>().ShotDefaultBullet (Bullet, ContadorPuntos, PuntoDisparo, RotationCañon, Player);

			}

			TextArma.text = "Arma Normal";

		}

		if (TipoArma == 1) {

			if (Disparar) {

				CañonObject.GetComponent<RapidShots>().RapidShotsBullet (Bullet, ContadorPuntos, PuntoDisparo, RotationCañon, Player, TextArma);

				if (CañonObject.GetComponent<RapidShots>().EndShot()) {

					TipoArma = 0;

				}

			}

		}

		if (TipoArma == 2) {
			
			if (Disparar) {
				
				CañonObject.GetComponent<TriShots>().TriShotsBullet (Bullet, ContadorPuntos, PuntoDisparo, RotationCañon, Player, TextArma);
				
				if (CañonObject.GetComponent<TriShots>().EndShot()) {
					
					TipoArma = 0;
					
				}
				
			}
			
		}

		if (TipoArma == 3) {

			if (Disparar) {
				
				CañonObject.GetComponent<Lacer>().LacerShots(true, Player, TextArma);
				
				if (CañonObject.GetComponent<Lacer>().EndShot()) {
					
					TipoArma = 0;

					CañonObject.GetComponent<Lacer>().LacerShots(false, Player, TextArma);
					
				}
				
			}
			else
			{

				CañonObject.GetComponent<Lacer>().LacerShots(false, Player, TextArma);

			}
			
		}

		if (TipoArma == 4) {
			
			if (Disparar) {
				
				CañonObject.GetComponent<RafagaShots>().RafagaShotsBullet (Bullet, ContadorPuntos, PuntoDisparo, RotationCañon, Player, TextArma);
				
				if (CañonObject.GetComponent<RafagaShots>().EndShot()) {
					
					TipoArma = 0;
					
				}
				
			}
			
		}

		////////////////////////////////////////////////////////////////////////////////////
		// Armas Secundarias

		if (TipoArma2 == 0) 
		{
			
			TextArma2.text = "Sin Arma Secundaria";
			
		}

		if (TipoArma2 == 1) {

			TextArma2.text = "Torreta";

			DropperObject.SetActive(true);
			
			if (Disparar2) {
				
				newObject = (GameObject)Instantiate (Torreta);
				newObject.transform.position = Dropper.transform.position;

				newObject.GetComponent<Torreta>().TorretaDrop(Player, ContadorPuntos);

				TipoArma2 = 0;

				DropperObject.SetActive(false);
				
			}
			
		}

		if (TipoArma2 == 2) {

			TextArma2.text = "Mina";

			DropperObject.SetActive(true);
			
			if (Disparar2) {
				
				newObject = (GameObject)Instantiate (Mina);
				newObject.transform.position = Dropper.transform.position;
				
				newObject.GetComponent<Mina>().MinaDrop(Player, ContadorPuntos);
				
				TipoArma2 = 0;

				DropperObject.SetActive(false);
				
			}
			
		}

		if (TipoArma2 == 3) {

			TextArma2.text = "Onda de Balas";
			
			if (Disparar2) {
				
				CañonObject.GetComponent<OndaBullets>().Shot(Bullet, ContadorPuntos, CañonObject, Player);
				
				TipoArma2 = 0;
				
			}
			
		}

		if (TipoArma2 == 4) {
			
			if (Disparar2) {
				
				CañonObject.GetComponent<DesviaBalas>().DesviaBalasShots(Player, TextArma2);
				
				if (CañonObject.GetComponent<DesviaBalas>().EndShot()) {
					
					TipoArma2 = 0;
					
				}
				
			}
			else
			{

				CañonObject.GetComponent<DesviaBalas>().Stop();

			}
			
		}

		if (TipoArma2 == 5) {
			
			TextArma2.text = "Barricada " + (4 - subQuality) + " / 4";

			DropperObject.SetActive(true);
			
			if (Disparar2) {

				if (subDisparo2) {

					newObject = (GameObject)Instantiate (Barricada);
					newObject.transform.position = new Vector3 (Dropper.transform.position.x, 0.95f, Dropper.transform.position.z);
					newObject.transform.rotation = Quaternion.Euler( new Vector3 (0, RotationCañon, 0));
					
					newObject.GetComponent<Barricada>().BarricadaDrop(Player, ContadorPuntos);
					
					subQuality = subQuality + 1;

					subDisparo2 = false;

				}
				
			}
			else
			{

				subDisparo2 = true;

			}

			if (subQuality >= 4) {

				subQuality = 0;

				TipoArma2 = 0;

				subDisparo2 = false;

				DropperObject.SetActive(false);

			}
			
		}

		if (TipoArma2 == 6) {
			
			TextArma2.text = "Atrae Balas";
			
			DropperObject.SetActive(true);
			
			if (Disparar2) {
				
				newObject = (GameObject)Instantiate (AtraeBalas);
				newObject.transform.position = Dropper.transform.position;
				
				newObject.GetComponent<AtraeBalas>().AtraeBalasDrop(Player, ContadorPuntos);
				
				TipoArma2 = 0;
				
				DropperObject.SetActive(false);
				
			}
			
		}

		////////////////////////////////////////////////////////////////////////////////////
		// Balas Especiales

		if (TipoBullet == 0) 
		{
			
			TextBullet.text = "Balas Simples";

			Bullet = BulletDefault;
			
		}

		if (TipoBullet == 1) 
		{
			
			TextBullet.text = "Balas Grandes";
			
			Bullet = BulletGrande;
			
		}

		if (TipoBullet == 2) 
		{
			
			TextBullet.text = "Balas Espectrales";
			
			Bullet = BulletEspectral;
			
		}

		if (TipoBullet == 3) 
		{
			
			TextBullet.text = "Balas Fragmentadoras";
			
			Bullet = BulletFragmentadora;
			
		}
		
		if (TipoBullet == 4) 
		{
			
			TextBullet.text = "Balas Teledirigidas";
			
			Bullet = BulletTeledirigida;
			
		}
		
		if (TipoBullet == 5) 
		{
			
			TextBullet.text = "Balas que Rebotan";
			
			Bullet = bulletRebota;
			
		}

		if (BulletActual != 0) {

			TipoBullet = BulletActual;

		}

		if (TipoBullet != 0 && BulletActual == 0) {
			
			if (subTimeBullet < TimeBulletEspeciales) {
				
				subTimeBullet = subTimeBullet + Time.deltaTime;
				
			}
			
			if (subTimeBullet >= TimeBulletEspeciales) {
				
				TipoBullet = 0;
				
				subTimeBullet = 0;
				
			}

			TextBullet.text += " " + Mathf.Round(TimeBulletEspeciales - subTimeBullet);
			
		}

	}

}
