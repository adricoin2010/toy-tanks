﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CountPoints : MonoBehaviour {

	public Text SetTime;
	public Text TextTime;
	public Text PuntosTotales;
	public Text PointPlayer1;
	public Text PointPlayer2;
	public Text PointPlayer3;
	public Text PointPlayer4;

	float GameTime = 200;
	int[] Players;
	bool Play = false;
	float subGameTime;

	public void PlayGame () {

		Play = true;

		Players[0] = 0;
		Players[1] = 0;
		Players[2] = 0;
		Players[3] = 0;

	}

	public void SalirPartida () {

		Play = false;

	}

	public void MenosTiempo () {

		if (GameTime > 50)
			GameTime = GameTime - 50;

	}

	public void MasTiempo () {

		GameTime = GameTime + 50;

	}

	public void AddPoint (int player) 
	{

		Players [player] = Players [player] + 1;

	}

	public bool EndGame () {

		return Play;

	}

	void Start () {
	
		Players = new int[4];

	}

	void Update () {
	
		if (Play) {

			PointPlayer1.text = Players [0] + "  ";
			PointPlayer2.text = Players [1] + "  ";
			PointPlayer3.text = Players [2] + "  ";
			PointPlayer4.text = Players [3] + "  ";

			if (subGameTime < GameTime) {
			
				subGameTime = subGameTime + Time.deltaTime;
			
			}

			TextTime.text = "Quedan " + Mathf.Round(GameTime - subGameTime) + " Segundos";

			if (subGameTime >= GameTime) {

				Play = false;

			}

		}
 		else
		{

			Play = false;

			subGameTime = 0;

			SetTime.text = "Tiempo: " + GameTime + " Segundos";

		}

		PuntosTotales.text = "Player 1 = " + Players [0] + "\nPlayer 2 = " + Players [1] + "\nPlayer 3 = " + Players [2] + "\nPlayer 4 = " + Players [3];

	}

}
