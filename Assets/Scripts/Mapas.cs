﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Mapas : MonoBehaviour {

	public Text NombreMapa;
	public int NumeroMapas;
	public GameObject Mapa1;
	public GameObject Mapa2;

	Vector3[] Tanques;
	int BufferMap;
	bool Activated;

	public void PreviousMap () {

		BufferMap = BufferMap - 1;

	}

	public void NextMap () {

		BufferMap = BufferMap + 1;

	}

	public Vector3 ReturnCheckPoint (int Player) {

		return Tanques[Player];

	}

	public void ElegirMapa (bool status) {

		Activated = status;

	}

	void Start () {
	
		Tanques = new Vector3[4];

		NumeroMapas = NumeroMapas - 1;

	}

	void Update () {

		if (Activated) {

			if (BufferMap > NumeroMapas) BufferMap = 0;
			if (BufferMap < 0) BufferMap = NumeroMapas;

			if (BufferMap == 0) {
				
				Mapa1.SetActive(true);
				NombreMapa.text = "Mapa 1";
				
				Tanques[0] = new Vector3(-15, 2, 10);
				Tanques[1] = new Vector3(15, 2, 10);
				Tanques[2] = new Vector3(-15, 2, -10);
				Tanques[3] = new Vector3(15, 2, -10);
				
			}
			else
			{
				
				Mapa1.SetActive(false);
				
			}

			if (BufferMap == 1) {
				
				Mapa2.SetActive(true);
				NombreMapa.text = "Mapa 2";
				
				Tanques[0] = new Vector3(-15, 2, 10);
				Tanques[1] = new Vector3(15, 2, 10);
				Tanques[2] = new Vector3(-15, 2, -10);
				Tanques[3] = new Vector3(15, 2, -10);
				
			}
			else
			{
				
				Mapa2.SetActive(false);
				
			}

		}

	}
}
