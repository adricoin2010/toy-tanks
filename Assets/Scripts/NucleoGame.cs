﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NucleoGame : MonoBehaviour {

	public CountPoints ContadorPuntos;
	public Mapas GestorMapas;
	public GameObject MenuInicio;
	public GameObject InterfazJuego;
	public GameObject ButtonObjectJugar;
	public GameObject ButtonObjectPlay;
	
	[Space(3)]
	[Header("Tanque Prefab")]

	//public GameObject TanquePrefab;
	GameObject TanquePrefab;

	[Space(3)]
	[Header("Tanque 1")]
	public GameObject Interfaz1;
	public GameObject HealthBar1;
	public Text TextArma1;
	public Text TextSecundaria1;
	public Text TextBullet1;

	[Space(3)]
	[Header("Tanque 2")]
	public GameObject Interfaz2;
	public GameObject HealthBar2;
	public Text TextArma2;
	public Text TextSecundaria2;
	public Text TextBullet2;

	[Space(3)]
	[Header("Tanque 3")]
	public GameObject Interfaz3;
	public GameObject HealthBar3;
	public Text TextArma3;
	public Text TextSecundaria3;
	public Text TextBullet3;

	[Space(3)]
	[Header("Tanque 4")]
	public GameObject Interfaz4;
	public GameObject HealthBar4;
	public Text TextArma4;
	public Text TextSecundaria4;
	public Text TextBullet4;

	GameObject[] newTanques;
	bool Jugar;
	bool Play;
	int Players;

	public void ButtonJugar() {

		Jugar = true;

	}

	public void ButtonPlay(int players) {

		Players = players;
		Play = true;

	}

	public void Exit() {

		Application.Quit ();

	}

	void Start () {

		newTanques = new GameObject[4];

		TanquePrefab = Resources.Load("Tanque") as GameObject;

	}

	void Update () {

		if (ContadorPuntos.EndGame() == false) {

			MenuInicio.SetActive(true);
			InterfazJuego.SetActive(false);
			GestorMapas.ElegirMapa(true);

			if (Jugar) {

				ButtonObjectJugar.SetActive(false);

				if (Play) {

					ContadorPuntos.PlayGame();

					if (Players == 2) {

						newTanques[0] = (GameObject)Instantiate (TanquePrefab);
						newTanques[0].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(0));
						newTanques[0].GetComponent<Tanque>().DatosPlayer(0, ContadorPuntos, Interfaz1, HealthBar1, TextArma1, TextSecundaria1, TextBullet1);
						
						newTanques[1] = (GameObject)Instantiate (TanquePrefab);
						newTanques[1].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(1));
						newTanques[1].GetComponent<Tanque>().DatosPlayer(1, ContadorPuntos, Interfaz2, HealthBar2, TextArma2, TextSecundaria2, TextBullet2);

					}

					if (Players == 3) {

						newTanques[0] = (GameObject)Instantiate (TanquePrefab);
						newTanques[0].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(0));
						newTanques[0].GetComponent<Tanque>().DatosPlayer(0, ContadorPuntos, Interfaz1, HealthBar1, TextArma1, TextSecundaria1, TextBullet1);
						
						newTanques[1] = (GameObject)Instantiate (TanquePrefab);
						newTanques[1].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(1));
						newTanques[1].GetComponent<Tanque>().DatosPlayer(1, ContadorPuntos, Interfaz2, HealthBar2, TextArma2, TextSecundaria2, TextBullet2);
						
						newTanques[2] = (GameObject)Instantiate (TanquePrefab);
						newTanques[2].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(2));
						newTanques[2].GetComponent<Tanque>().DatosPlayer(2, ContadorPuntos, Interfaz3, HealthBar3, TextArma3, TextSecundaria3, TextBullet3);

					}

					if (Players == 4) {

						newTanques[0] = (GameObject)Instantiate (TanquePrefab);
						newTanques[0].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(0));
						newTanques[0].GetComponent<Tanque>().DatosPlayer(0, ContadorPuntos, Interfaz1, HealthBar1, TextArma1, TextSecundaria1, TextBullet1);

						newTanques[1] = (GameObject)Instantiate (TanquePrefab);
						newTanques[1].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(1));
						newTanques[1].GetComponent<Tanque>().DatosPlayer(1, ContadorPuntos, Interfaz2, HealthBar2, TextArma2, TextSecundaria2, TextBullet2);

						newTanques[2] = (GameObject)Instantiate (TanquePrefab);
						newTanques[2].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(2));
						newTanques[2].GetComponent<Tanque>().DatosPlayer(2, ContadorPuntos, Interfaz3, HealthBar3, TextArma3, TextSecundaria3, TextBullet3);

						newTanques[3] = (GameObject)Instantiate (TanquePrefab);
						newTanques[3].GetComponent<Health>().Spawn(GestorMapas.ReturnCheckPoint(3));
						newTanques[3].GetComponent<Tanque>().DatosPlayer(3, ContadorPuntos, Interfaz4, HealthBar4, TextArma4, TextSecundaria4, TextBullet4);

					}

				}
				else
				{

					ButtonObjectPlay.SetActive(true);

				}

			}
			else
			{

				ButtonObjectJugar.SetActive(true);
				ButtonObjectPlay.SetActive(false);

			}

		}
		else
		{

			MenuInicio.SetActive(false);
			InterfazJuego.SetActive(true);
			GestorMapas.ElegirMapa(false);

			Jugar = false;
			Play = false;

		}
	
	}

}
