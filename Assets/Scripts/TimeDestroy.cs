﻿using UnityEngine;
using System.Collections;

public class TimeDestroy : MonoBehaviour {

	public bool Activated;
	public float DestroyTime;
	public bool Particles;
	public float EmitterStop;

	float subDestroyTime;
	float subEmitterStop;
	
	void Update () {

		if (Activated) {

			if (subDestroyTime >= DestroyTime) {

				Destroy (gameObject);

			}

			if (subDestroyTime < DestroyTime) {
				
				subDestroyTime = subDestroyTime + Time.deltaTime * 1;
				
			}

			if (Particles) {

				if (subEmitterStop >= EmitterStop) {
					
					gameObject.GetComponent<ParticleEmitter>().emit = false;
					
				}

				if (subEmitterStop < EmitterStop) {

					subEmitterStop = subEmitterStop + Time.deltaTime * 1;

				}

			}

		}

	}

}
