﻿using UnityEngine;
using System.Collections;

public class CajaSecreta : MonoBehaviour {

	public GameObject CubeObject;
	public GameObject Luz;
	public float MinTime;
	public float MaxTime;

	bool Activated;
	bool Buffer;
	float BufferTime;
	float subBufferTime;
	int TipoObjeto;
	int TipoArma;

	void Start () {

		Activated = true;

		Buffer = true;

	}

	void OnTriggerStay (Collider enter) {

		if (Activated) {

			if (enter.tag == "Player") {

				if (TipoObjeto == 1) {
					
					enter.GetComponent<GestionArmas>().CambioArmaPrimaria(TipoArma);
					
				}
				else if (TipoObjeto == 2) {
					
					enter.GetComponent<GestionArmas>().CambioArmaSecundaria(TipoArma);
					
				}
				else if (TipoObjeto == 3) {
					
					enter.GetComponent<GestionArmas>().CambioBullet(TipoArma);
					
				}

				Activated = false;

				Buffer = true;

				BufferTime = Random.Range(MinTime, MaxTime);

				CubeObject.SetActive(false);

				Luz.SetActive(false);

			}

		}

	}

	void Update () {

		if (subBufferTime >= BufferTime && Buffer) {
			
			Activated = true;
			
		}
		
		if (subBufferTime < BufferTime && Buffer) {
			
			subBufferTime = subBufferTime + Time.deltaTime;
			
		}

		if (Activated && Buffer) {

			TipoObjeto = Random.Range(1,4);

			if (TipoObjeto == 1) {

				CubeObject.GetComponent<Renderer>().material = Resources.Load("Materials/Caja Secreta/Caja Secreta 1") as Material;

				Luz.GetComponent<Light>().color = Color.yellow;

				TipoArma = Random.Range(1,5);

			}
			else if (TipoObjeto == 2) {

				CubeObject.GetComponent<Renderer>().material = Resources.Load("Materials/Caja Secreta/Caja Secreta 2") as Material;
				
				Luz.GetComponent<Light>().color = Color.magenta;

				TipoArma = Random.Range(1,7);

			}
			else if (TipoObjeto == 3) {

				CubeObject.GetComponent<Renderer>().material = Resources.Load("Materials/Caja Secreta/Caja Secreta 3") as Material;
				
				Luz.GetComponent<Light>().color = Color.red;

				TipoArma = Random.Range(1,6);

			}

			CubeObject.SetActive(true);
			
			Luz.SetActive(true);

			Buffer = false;

			subBufferTime = 0;

		}

		if (Activated) {

			CubeObject.transform.Rotate(new Vector3(0, 3, 0));

		}

	}

}
