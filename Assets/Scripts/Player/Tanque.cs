﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using XInputDotNetPure;

public class Tanque : MonoBehaviour {
	
	public GameObject TanqueObject;
    public GameObject CuerpoObject;
	public Renderer CuerpoMesh;
    public GameObject CañonObject;
	public Renderer CañonMesh;
	public Renderer LacerMesh;
    public int Gravity;
    public float Vel;

	int Player;
	CountPoints ContadorPuntos;
	GameObject Interfaz;

    GamePadState Mando;

	bool Activated = true;
    float RotationCuerpo;
    float RotationCañon;

    Vector3 PlayerMove;
    Vector2 JoystickIzquierdo;
    Vector2 JoystickDerecho;

	bool Ralentizado = false;
	bool Acelerado = false;

	public void DatosPlayer (int player, CountPoints contadorPuntos, GameObject interfaz, GameObject HealthBar, Text TextArma, Text TextArma2, Text TextBullet) {

		Player = player;
		ContadorPuntos = contadorPuntos;
		Interfaz = interfaz;

		TanqueObject.GetComponent<GestionArmas>().DatosPlayer(Player, ContadorPuntos, Interfaz, TextArma, TextArma2, TextBullet);
		TanqueObject.GetComponent<Health>().DatosPlayer(HealthBar, ContadorPuntos);

		CuerpoMesh.material = Resources.Load ("Materials/Players/Tanque " + (Player + 1)) as Material;
		CañonMesh.material = Resources.Load ("Materials/Players/Cañon " + (Player + 1)) as Material;
		LacerMesh.material = Resources.Load ("Materials/Lacers/Lacer " + (Player + 1)) as Material;
		
		Interfaz.SetActive(true);

	}

	public void ActivatedTanque (bool status) {

		Activated = status;

	}

	public int returnPlayer () {

		return Player;

	}

	public void Ralentizar () {

		Ralentizado = true;

	}

	public void Acelerar () {
		
		Acelerado = true;
		
	}

	void Update () {

		if (ContadorPuntos.EndGame () == false) {

			Interfaz.SetActive(false);

			CañonObject.GetComponent<Lacer>().DestroyFlare();

			Destroy(TanqueObject);

		}

		if (Activated) {

			Interfaz.SetActive(true);

			Mando = GamePad.GetState ((PlayerIndex)Player);

			GamePad.SetVibration((PlayerIndex)Player, Mando.Triggers.Left, Mando.Triggers.Right);

			JoystickIzquierdo.x = Mando.ThumbSticks.Left.X;
			JoystickIzquierdo.y = Mando.ThumbSticks.Left.Y;

			JoystickDerecho.x = Mando.ThumbSticks.Right.X;
			JoystickDerecho.y = Mando.ThumbSticks.Right.Y;

			if (JoystickIzquierdo.x != 0 || JoystickIzquierdo.y != 0) {

				RotationCuerpo = Mathf.Rad2Deg * Mathf.Atan2 (JoystickIzquierdo.x, JoystickIzquierdo.y);

			}

			if (JoystickDerecho.x != 0 || JoystickDerecho.y != 0) {

				RotationCañon = Mathf.Rad2Deg * Mathf.Atan2 (JoystickDerecho.x, JoystickDerecho.y);

			}

			CuerpoObject.transform.localRotation = Quaternion.Euler (0, RotationCuerpo, 0);
			CañonObject.transform.localRotation = Quaternion.Euler (0, RotationCañon, 0);

			TanqueObject.GetComponent<GestionArmas> ().ApuntarCañon (JoystickDerecho, RotationCañon);

			if (Mando.Buttons.RightShoulder == ButtonState.Pressed) {

				TanqueObject.GetComponent<GestionArmas> ().DispararArma (true);

			} 
			else 
			{

				TanqueObject.GetComponent<GestionArmas> ().DispararArma (false);

			}

			if (Mando.Buttons.LeftShoulder == ButtonState.Pressed) {
				
				TanqueObject.GetComponent<GestionArmas> ().DispararArmaSecundaria(true);
				
			} 
			else 
			{
				
				TanqueObject.GetComponent<GestionArmas> ().DispararArmaSecundaria(false);
				
			}

			if (Ralentizado && Acelerado) {

				PlayerMove.x = JoystickIzquierdo.x * Vel;
				PlayerMove.z = JoystickIzquierdo.y * Vel;

			}
			else if (Ralentizado) {

				PlayerMove.x = JoystickIzquierdo.x * (Vel - 3);
				PlayerMove.z = JoystickIzquierdo.y * (Vel - 3);

				Ralentizado = false;

			}
			else if (Acelerado) {

				PlayerMove.x = JoystickIzquierdo.x * (Vel + 3);
				PlayerMove.z = JoystickIzquierdo.y * (Vel + 3);

			}
			else
			{

				PlayerMove.x = JoystickIzquierdo.x * Vel;
				PlayerMove.z = JoystickIzquierdo.y * Vel;

			}

			if (TanqueObject.GetComponent<CharacterController>().isGrounded) {

				PlayerMove.y = 0;

			}

			PlayerMove.y -= Gravity * Time.deltaTime;
			TanqueObject.GetComponent<CharacterController>().Move (PlayerMove * Time.deltaTime);

			PlayerMove.x = 0;
			PlayerMove.z = 0;

		}
		else
		{

			Interfaz.SetActive(false);

		}

	}
}
