﻿using UnityEngine;
using System.Collections;

public class BulletAtraccion : MonoBehaviour {

	public GameObject Tanque;
	public float Force;
	public float Radius;

	void Update () {
	
		Collider[] colliders = Physics.OverlapSphere (transform.position, Radius);
		
		foreach (Collider hit in colliders) {
			
			if (hit.tag == "Bullet") {
				
				if (hit.GetComponent<Bullet>().isTeledirigido()) {

					if (hit.GetComponent<Bullet>().returnPlayer() != Tanque.GetComponent<Tanque>().returnPlayer()) {

						hit.GetComponent<Rigidbody>().AddExplosionForce(Force, transform.position, Radius);

					}
					
				}
				
			}
			
		}

	}

}
