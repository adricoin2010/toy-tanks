﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Health : MonoBehaviour {
	
	public GameObject TanqueObject;
	public GameObject ExplosionDeath;
	public GameObject SmokeSpawn;
	public bool Inmortal;

	GameObject HealthBar;
	CountPoints ContadorPuntos;
	GameObject newExplosion;
	GameObject newSmoke;
	int MaxHealth = 100;
	float BufferHealth;
	int PlayerAttack;
	Vector3 CheckPoint;

	int Player;
	float subTimeVibration;

	void Start () {
		
		BufferHealth = MaxHealth;
		
		Player = TanqueObject.GetComponent<Tanque>().returnPlayer();

		subTimeVibration = 1;
		
	}

	public void DatosPlayer (GameObject healthBar, CountPoints contador) {

		HealthBar = healthBar;
		ContadorPuntos = contador;
		
	}

	public void Damage (float status, int player) 
	{

		if (Inmortal == false) {

			BufferHealth = BufferHealth - status;
			PlayerAttack = player;

		}

		subTimeVibration = 0;

	}

	public void Botiquin () 
	{

		BufferHealth = BufferHealth + 25;

		if (BufferHealth > MaxHealth) {

			BufferHealth = MaxHealth;

		}

	}

	public void Spawn(Vector3 spawnposition) {

		CheckPoint = spawnposition;

		TanqueObject.transform.position = spawnposition;

		BufferHealth = MaxHealth;

	}

	public void Respawn() {

		TanqueObject.transform.position = CheckPoint;

	}

	void Update () {
		
		if (subTimeVibration < 0.5) {
			
			subTimeVibration = subTimeVibration + Time.deltaTime;

			GamePad.SetVibration ((PlayerIndex)Player, 1, 1);
			
		}

		if (BufferHealth <= 0) 
		{

			ContadorPuntos.AddPoint(PlayerAttack);

			newExplosion = (GameObject)Instantiate (ExplosionDeath);
			newExplosion.transform.position = TanqueObject.transform.position;

			TanqueObject.transform.position = CheckPoint;

			BufferHealth = MaxHealth;

			newSmoke = (GameObject)Instantiate (SmokeSpawn);
			newSmoke.transform.position = TanqueObject.transform.position;

		}
		else
		{

			HealthBar.transform.localScale = new Vector3 (BufferHealth / 100, 1, 1);

		}

	}

}
